package sheridan;

import sheridan.LiquourType;

import java.util.ArrayList;
import java.util.List;

public class LiquourService {
	

    public List<String> getAvailableBrands(LiquourType type){

        List<String> brands = new ArrayList( );

        if(type.equals(LiquourType.Wine)){
            brands.add("Adrianna Vineyard");
            brands.add(("J. P. Chenet"));

        }else if(type.equals(LiquourType.Whisky)){
            brands.add("Glenfiddich");
            brands.add("Johnnie Walker");

        }else if(type.equals(LiquourType.Beer)){
            brands.add("Corona");
            brands.add("Budwiser");
            brands.add("Heineken");
            brands.add("Coors");
            brands.add("Canadian");

        } else {
            brands.add("No Brand Available");
        }
    return brands;
    }
}
